/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tare1semana3;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

/**
 *
 * @author usuario
 */
public class Diccionario {

    Map<String, Integer> diccionario = new HashMap();

    Scanner ingreso = new Scanner(System.in);//Este va a ser para la cantidad de personas que desea ingresar, y para la edad, básicamente para los números que ingresen.
    Scanner lectura = new Scanner(System.in);//Nombre de la persona.

    int numeropersonas, edad, gerald = 1, j;
    String nombre, red = "\033[31m", blue = "\033[34m";

    private void operaciones() {

        //Este for es para ir pasando el número de veces que el usuario digito, la candidad de personas que desea ingresar.
        for (int i = 0; i < numeropersonas; i++) {
            System.out.print("\nDigite el nombre: ");
            nombre = lectura.nextLine();
            System.out.print("\nDigite la edad: ");
            edad = ingreso.nextInt();
            diccionario.put(nombre, edad);

        }

        Set<String> i = diccionario.keySet();
        for (String llave : i) {
            int j = diccionario.get(llave);
            if (j % 2 == 0) {
                System.out.print(llave);
                System.out.println(" "+ blue+j + "\u001B[0m");

            } else {
                System.out.print(llave);
                System.out.println(" " + red + j + "\u001B[0m");

            }

        }

    }

    public void menu() {

        System.out.println("\nPrograma que permite ingresar nombres y edades de personas, este programa fue hecho con un diccionario.\n");

        System.out.print("\nCuantas personas desea ingresar: ");

        numeropersonas = ingreso.nextInt();

        operaciones();
    }

}
